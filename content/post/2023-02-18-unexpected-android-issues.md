---
title: "Unexpected Android Issues"
Date: 2023-02-18
tags: ["android", "dependencies", "internet"]
draft: false
---

A user asked me to install a work-related app for him from the Google Play Store.
The installation was fine, it was the "making it run" part that was tricky.
<!--more-->

### Installation was a breeze

**00m00s**
I brought the tablet home so that I could take advantage of a faster and cheaper
connection in the late hours. This is worth the effort where I live, even for an
app of maybe 100 MB, because the network is always congested and barely usable
during work hours. So I had the app downloaded and installed in about 10 minutes.

### Tapping the icon to launch it was no problem

**10m13s**
Once installed I tapped the icon to open the app, and, after quite a slow launch
(this is not a top-of-the-line tablet, it's a MediaTek *something-or-other* from
China, and it's running Android 4, in 2023), I got nothing but a plain white screen.

### Getting it to run, however...

Something in the back of my mind told me I had seen this before, maybe 2+ years
ago. Someting else in the back of my mind told me I should just let it sit for
awhile to see if it's just extremely slow to start the first time because of the
device's limited processing power. So I waited. 1 minute... 3 minutes... 10 minutes...
30 minutes later I decided that was a dead end.

**42m17s**
*Then* I remembered something about Android System Webview being an issue the
last time I saw this. So I thought it wise to go the Play Store and update *that*
app before trying to make *this* one run. (More about Android System Webview in
[this MakeUseOf article](https://www.makeuseof.com/what-is-android-system-webview/).)

**43m34s**
So I find it in the Play Store and click "Update". Then, this:

![Google Play error DF DFERH 01](/img/Google-Play-Error-DF-DFERH-01.jpg)

Oops! Thankfully others have walked down this DeF DeFERHring path before, and I
was able to find out that a Play Store update should solve the problem. Have *you*
ever tried to manually update the Play Store? Did *you* know that you can't do it
from...the Play Store?

On my phone (Android 11) there's an "Update Play Store" link in Play Store > Settings:

![Play Store settings screenshot](/img/screenshot_Google_Play_Store.jpg)

**47m06s**
But on this tablet there was nothing show there but the version number. Thankfully,
occasionally needing to [unlock "Developer Options"](https://duckduckgo.com/?q=android+unlock+developer+options&t=fpas&atb=v92-1&ia=web) in the past has conditioned me to try
just tapping repeatedly on things to see if it does something useful. Turns out,
that's exactly what I needed to do to make Play Store head out and search for an
update!

**50m28s**
Unfortunately, it failed the first time. Actually the first 5 times, since
I hit the version entry 5 times in a row, which made launch 5 update search processes.
But, when the connection was good again I was able to complete the update and
finally get back to trying to update Android System Webview.

**01h03m45s**
The app update downloaded and installed successfully. And when I then launched
the original app that was the point of this whole jaunt through the wilderness,
it open fully and successfully after only a few seconds. Whew!

### Takeaways

So what's the lesson here?
1. Android apps *do* actually have some dependencies, and developers may or may
not realize that if something like Webview is outdated, then their own app may
not run correctly.
1. Google (and Microsoft and Apple) assumes that *everyone* has fast, consistent
internet, and that we can and should all just have our operating systems and apps
set to always auto-update to avoid these kinds of problems. And to rub it in,
Google is notorious for pushing practically *daily* updates for nearly every single
one of its apps.
1. A lot of time has to be spent by tech support in countries with slow, expensive,
and intermittent internet trying to find workarounds for these kinds of systems
that simply take for granted that users have a good, fast, cheap internet connection.
You're welcome.
