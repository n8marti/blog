---
title: Repair Damaged DOCX and ODT Files
Date: 2021-11-20
tags: ["recovery", "USB drive", "CLI", "open source"]
draft: false
---

A user came to me the other day because he couldn't open an important document--
a report he had been writing to inform funders of his project's progress. I
thought the file was a goner. LibreOffice couldn't repair it or open it. My
guess was that the file got corrupted, likely due to hastily removing the USB
flash drive that contained it before the file had finished getting written to disk.
I told him I'd take a look at it and see if I could figure anything out.
<!--more-->

Maybe I'll discuss the love/hate relationship I have with USB drives in another
post, but I want to share here the relatively simple way I was able to recover
the DOCX document when LibreOffice wasn't able to automatically.

### Some background info
Little do most people know that DOCX files (standard format for Microsoft Word)
and ODT files (standard format for LibreOffice Writer) are actually ZIP folders
containing mostly XML files. In fact,

>The ZIP-based package for any ODF file contains, at a minimum, five files: a
one-line mimetype file containing a single text string; content.xml; styles.xml; meta.xml; and settings.xml.  
[ODT Format, Library of Congress](https://www.loc.gov/preservation/digital/formats/fdd/fdd000428.shtml)

This means that tools that can be used to read and manipulate ZIP files can also
be used to read and manipulate DOCX and ODT files.

### The fix
So this is helpful knowledge when wanting to examine or repair a misbehaving DOCX
or ODT file. So here are the steps I took:
1. Whip out your computer's terminal app. (Ctrl+Alt+T on Ubuntu)
2. Rename the file to use a .zip extension (FILE).
```bash
cp FILE.{docx,zip} # copy it to preserve the original
```
3. Use the "zip" command's "-F" and "-FF" flags to repair the archive:
```bash
zip -F FILE.zip --out NEWFILE.zip # try this first--it's more reliable
zip -FF FILE.zip --out NEWFILE.zip # less reliable but more powerful
```
4. Rename NEWFILE to use the correct extension, .docx or .odt:
```bash
cp NEWFILE.{zip,docx} # copy it to preserve the ZIP, just in case
```
5. See if you can now open the file with LibreOffice or Word.

In my case, all of the user's file's contents were undamaged. So once the ZIP structure
was repaired the user was able to confirm that the document itself was intact.
He was so happy (and so was I!) that he was able to complete the multi-page
report without having to start all over again.
