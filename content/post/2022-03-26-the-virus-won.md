---
title: The Virus Won
Date: 2022-03-26
tags: ["virus", "Windows", "Linux"]
draft: false
---

A user has had to have Windows reinstalled 3 times over the last 2 months. Each
time he came to me, the system was in an unbootable state, probably due to a virus of some sort,
and Windows Boot Repair couldn't fix it. Neither were there any restore points available.
<!--more-->
At some point it ends up being faster just to reinstall Windows, but reinstalling
Windows is still a multi-hour process, involving:
- backing up the user profile folder
- installing the system
- configuring the system (simplified on my end thanks to a powershell script)
- and restoring the user's files

It puts the user out of work for at least a day before all is said and done. And
this happened to this particular user (and our IT Dept.) **3 times** over the last **2 months**!

Well, the same user came to me the other day with *yet another* boot problem.
This time, Windows Boot Loader didn't even recognize a bootable drive. My
colleague and I went through the list of possibilities:
- Was the hard drive somehow disconnected from the motherboard?
  *No. It was directly connected and no sign of being separated.*
- Was the disk corrupted or unreadable?
  *No. It could be read by a different device using an adapter. We took this opportunity to backup the user's files, though.*
- Would it boot if installed in a different computer?
  *Yes! But why? Maybe it's showing signs of age and just boots SOMETIMES?*
- Would it boot at least once in the original laptop if we try several times in a row?
  *No.*
- Are the laptop's boot settings (UEFI vs. Legacy, SecureBoot) correct?
  *No! The laptop was set to boot in Legacy mode while the Windows system had been installed in UEFI mode.*

There was our solution: reinstate UEFI boot mode and re-enable SecureBoot. Problem solved...

...Until the next day, when he came back to us ***with the same problem!***

Okay, the first time I wondered if this user had sought help from someone else
before coming to me, and maybe this *someone else* had changed boot settings
without really knowing what he was doing. That kind of thing happens sometimes:
I'm not in the office every day, and sometimes people look farther afield for
help. But *the second time!?!?* There's no way anyone had time to mess around
with the system and accidentally screw something up.

By this time the alarm bells started going off in my head that there must be some
kind of virus shenanigans going on here. The user had ESET installed, and most
likely up to date, but he's known for taking files on USB flash drives to neighborhood
print kiosks to get documents printed, and they are a known source of viruses here.
*There must be some kind of virus or malware that was changing his system's boot settings!*

He was tired of the struggle and just wanted a new computer. I tried to explain
to him that the source of his problems was more likely to be viruses than the
particular hardware he was using, and that a new computer wouldn't really solve
anything. The only path forward was to reinstall the system *again*. So that's when
I proposed to him that he give Wasta-Linux a try.

[Wasta-Linux](https://wastalinux.org) is an Ubuntu-based operating system that
has been honed specifically for those who work in the field of language development.
It's main benefits are:
- less internet-dependent than Windows (more control over the timing and method of receiving updates)
- resistant to viruses
- no license to purchase or manage
- less demanding of system resources (hard drive space and memory)
- most peripheral devices have built-in driver support in the Linux kernel
- high customizability (I have created a software package that fully configures the system and installs location-specific software.)
A couple of drawbacks are:
- some software is only built for Windows and/or MacOS (but most used in our org. is cross-platform)
- some adaptations have to be made when learning a new system

So, the virus won. But so did Linux: the user dumped Windows and now has Wasta-Linux installed.
