---
title: How Much Storage Does Your Phone Have?
Date: 2021-12-11
tags: ["smartphone", "storage"]
draft: false
---

Someone came to me once wondering why his smartphone couldn't hold any more apps,
even though it had 8 GB of storage and he had put next to nothing on it.
<!--more-->

### Quick background info
You can get smartphones here for very reasonable prices. Some cost as little as
about $50. But the price often belies various shortcuts and cost savings taken
by manufacturers. One such shortcut is skimping on built-in storage space.

### Back to the story
So this user loaned me his phone for the afternoon so I could make time to
examine why it said he was out of space, even with only a couple of apps and a
couple of photos on it.

I first checked the Storage info in the Settings menu, and it clearly said he
had used nearly 100% of his storage space. Then I checked how many apps were
installed (base apps, < 10 total), how many pictures he had on it (< 3), and
how many files were in the Download folder (zero). And the storage used by the
system was shown to be well short the of total.

So then I experimented a little bit. I grabbed a 5 MB MP3 file from my computer
and copied it to his phone. Interestingly enough, the amount of available space
decreased by *10 MB*.

*That was interesting.*

I removed that file and copied over a 20 MB video. The amount of available space
decreased by *40 MB*.

*Oh, now I get it.*

So, the OS was reporting a Storage volume of 8 GB when it actually
only provided 4 GB. And to make the accounting work out, every byte added to storage
was double-counted so that the system could claim *8 GB* of storage in
the Settings and show the correct *relative* disk usage as files were added, but
it reality it only had *4 GB* of space. The user would have no idea unless he
did what I did and compared the actual size of files added with how much the
reported disk usage changed with each file added. Go figure.
