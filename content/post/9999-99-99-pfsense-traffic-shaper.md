---
title: How to Use Traffic Shaper on a pfSense Firewall
Date: 9999-99-99
tags: ["firewall", "network", "internet", "pfSense", "open source"]
draft: true
---

I recently wanted to see if I could get better network performance while using a
pfSense firewall. Here's what I learned while configuring and testing the built-in
Traffic Shaper feature.
<!--more-->

### Goal
Use Traffic Shaper to allocate how available internet (WAN) bandwidth gets used.

### Conditions
- Traffic Shaper requires that a hard limit be set for the total amount of bandwidth
  allowed to and from the WAN. In my case for testing purposes I use a very
  restrictive limit of 1024 Kbps, but this should be set to the allotted bandwidth
  given to you by your ISP, if your service includes such a thing, or maybe the
  average bandwidth your ISP provides during the work day.
- Fine control over bandwidth management requires relatively stable and predictable
  IP addresses or ports for the traffic being managed. Unfortunately, this makes
  traffic shaping somewhat unreliable on big CDN sites who make use of myriad domain
  names that are hard to predict and that can resolve to frequently-changing,
  undocumented IP addresses.

My purpose here is not to give a comprehensive guide but to show the basic steps
I've used for configuring Traffic Shaper and evaluating the results. I would
call it a *proof of concept* more than a guide.

### Initial Setup
- Wizard config
- 4 queues

### Configuration
- queues
  - qHigh: GMail
  - qLow: Facebook
  - qDefault
  - qTesting
- aliases
  - GMail
  - Facebook

### Testing
- firewall rules
  - sort by queue, add dividers for easier comprehension
  - put one rule at a time in testing queue
- queue status
  - observe whether or not there's traffic in the testing queue
  - try loading pages from sites in both queues at the same time and observe the
    bandwidth usage

### References
- [Traffic Shaper offical documentation](https://docs.netgate.com/pfsense/en/latest/trafficshaper/index.html)
- [Sample config file](img/config.xml)
