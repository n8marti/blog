---
title: Keeping Blog Ideas in One Place
#subtitle: Subtitle
Date: 2022-12-31
tags: ["ideas", "blog"]
draft: true
#disqus_identifier:
#disqus_title:
#disqus_url:

---

### Topic Ideas
<!--more-->

### MP3 Files and Cheap Media Players
- rsync for file updating
- disk order is important
- fatsort can do it

### Remote Desktop Support via Mobile Phone in a 2nd Language

#### Goal:
Prepare a physically isolated team for remote work (audio calls, screensharing, etc.) with colleagues.

#### Components:
- ensure adequate backup power
- ensure adequate 3G internet device
- ensure method of purchasing credit
- ensure installation of needed software
- ensure proper software registration and user accounts
- ensure economic use of upload & download internet data
- ensure users' training and familiarity with software


### Flood fallout
- Disassembled, cleaned, and dried components previously on the floor:
  - desktop server
  - Synology
  - 2 power strips
- Failed desktop server power supply (unifi controller)
- Failed Synology power supply (replaced with under-powered used one: lots of problems)
- Failed Synology HDD


### Evidence of a Virus?
- My computer is running very slowly
- I have random files missing
- Some of my Finance spreadsheets have been altered


### One Way to Ensure Protection of Work-Related Files
- synchronize Documents and Desktop folders to local server
- exclude file types likely to be large and unrelated to work
- make local and cloud backups of synced server folder
- execute this setup using a script executed on each user's system


### Making a Simple Static Website for Sharing Published Content
- upload content to Shared Google Drive folder
- create page in Google Sites
  - add shared Drive folder
  - add simple descriptive text
  - make page publicly accessible
- make content updates as necessary in Shared Drive folder; no need to worry about the page after setup


### USB Drives in the Developing World
- viruses
- "proper removal" by holding Esc while pulling out disk
- low capacity
- cheap quality
