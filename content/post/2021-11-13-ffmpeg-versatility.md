---
title: The Versatility of ffmpeg
Date: 2021-11-13
tags: ["video", "audio", "CLI", "open source"]
draft: false
---

There's a command line app called ffmpeg that can do a zillion things. So many
things, in fact, that its help menu is too dense to be very helpful to me, so I
just end up going to [DuckDuckGo](duckduckgo.com) to find out how to do whatever
audio- or video-related task I need to accomplish. For example, I searched
["ffmpeg convert to h265"](duckduckgo.com/?q=ffmpeg+convert+to+h265) to find out
how to re-encode a video using H265 encoding to reduce the file size.
<!--more-->
I've used ffmpeg for several video-related tasks over the last couple of months,
so I thought it would be worth giving a quick "demo" of the variety of things
I've used it for over such a short period of time.

### Find out useful properties of audio and video files
```bash
$ ffmpeg -i video.mp4
ffmpeg version 4.2.4-1ubuntu0.1 Copyright (c) 2000-2020 the FFmpeg developers
  built with gcc 9 (Ubuntu 9.3.0-10ubuntu2)
  configuration: [...]
  avcodec     configuration: [...]
  libavutil      56. 31.100 / 56. 31.100
  libavcodec     58. 54.100 / 58. 54.100
  libavformat    58. 29.100 / 58. 29.100
  libavdevice    58.  8.100 / 58.  8.100
  libavfilter     7. 57.100 /  7. 57.100
  libavresample   4.  0.  0 /  4.  0.  0
  libswscale      5.  5.100 /  5.  5.100
  libswresample   3.  5.100 /  3.  5.100
  libpostproc    55.  5.100 / 55.  5.100
Input #0, mov,mp4,m4a,3gp,3g2,mj2, from 'input-video.mp4':
  Metadata:
    major_brand     : mp42
    minor_version   : 512
    compatible_brands: isomiso2avc1mp41
    creation_time   : 2021-02-02T08:06:41.000000Z
    title           : Title_1600x1024
    encoder         : HandBrake 1.3.1 2020032300
  Duration: 00:52:52.10, start: 0.000000, bitrate: 350 kb/s
    Chapter #0:0: start 0.000000, end 153.120000
    Metadata:
      title           : Recording Started
    Chapter #0:1: start 153.120000, end 2446.640000
    Metadata:
      title           : Sharing Started
    Chapter #0:2: start 2446.640000, end 3172.040000
    Metadata:
      title           : Sharing Stopped
    Stream #0:0(und): Video: h264 (Main) (avc1 / 0x31637661), yuv420p(tv, bt709), 720x480 [SAR 512:403 DAR 768:403], 182 kb/s, 25 fps, 25 tbr, 90k tbn, 180k tbc (default)
    Metadata:
      creation_time   : 2021-02-02T08:06:41.000000Z
      handler_name    : VideoHandler
    Stream #0:1(und): Audio: aac (LC) (mp4a / 0x6134706D), 32000 Hz, mono, fltp, 161 kb/s (default)
    Metadata:
      creation_time   : 2021-02-02T08:06:41.000000Z
      handler_name    : Mono
    Stream #0:2(eng): Data: bin_data (text / 0x74786574)
    Metadata:
      creation_time   : 2021-02-02T08:06:41.000000Z
      handler_name    : SubtitleHandler
At least one output file must be specified
$
```
You can see that this file, video.mp4, has the following properties:
Property | Value | Info Source
--- | --- | ---
duration | 52m 52.10s | ..Duration:
total bitrate | 350 KB/s | ..Duration:
video codec | h264 | ....Stream #0:0(und):
resolution | 720x480 | ....Stream #0:0(und):
video bitrate | 182 KB/s| ....Stream #0:0(und):
framerate | 25 fps | ....Stream #0:0(und):
audio codec | aac | ....Stream #0:1(und):
no. of channels | mono | ....Stream #0:1(und):
audio bitrate | 161 KB/s | ....Stream #0:1(und):
subtitles? | yes | ....Stream #0:2(eng):
| | | ......handler_name: SubtitleHandler

---

### Standardize properties of audio files and videos
```bash
$ du -h video2.mp4 # check file size before conversion
105M	video2.mp4
$ ffmpeg -i video2.mp4 -vcodec libx265 -crf 28 video2.x265.mp4
[...]
$ du -h video2.x265.mp4 # check file size after conversion
16M	video2.x265.mp4
$
```
Convert this video to H.265 from H.264 to reduce file size from 105MB
to 16MB without sacrificing quality. Here I also used '-crf' to set a constant
quality rate.

---

### "Crop" videos to remove unwanted frames
```bash
$ ffmpeg -i video2.x265.mp4 -ss 1:05 -to 3:00 -c:v copy -c:a copy video2.x265.mp4
[...]
$
```
This "cropped" the same video2.x265.mp4 from above to save only the frames from
1m 05s to 3m 00s.

---

### Reintegrate audio stream into video file
```bash
$ ffmpeg -i video2.x265.mp4 -i video2-enhanced-audio.wav -map 0:v -map 1:a -c:v copy -shortest input-video.x265.enh.mp4
[...]
$
```
After cropping video2.x265.mp4 I used another program to reduce background noise
and amplify the speakers' voices. I exported that as video2-enhanced-audio.wav,
then reintegrated it into the video file.

---

### Burn subtitles file into video
```bash
$ ffmpeg -i video3.mp4 -vf subtitles=captions-FR.srt video3.s.mp4
[...]
$
```
And finally, for video3.mp4 I translated a subtitles file in French using
[DeepL](deepl.com), then burned the subtitles into the video file so they showed
automatically without any need for the user to fiddle with any settings.
