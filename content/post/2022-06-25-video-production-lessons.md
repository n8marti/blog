---
title: Video Production on the Cheap
Date: 2022-06-25
tags: ["video", "conference", "open-source"]
draft: true
---

I recently helped organize a small, 1-day conference. My job was to make sure all
the technical parts came together, such as getting audio and video footage and
running the projector and sound system. Afterwards, I plopped all the media
content onto my laptop and worked to put together a consolidated video of the
day's events.

This process was interesting because:
1. We don't have reliable electricity where I am.
1. I don't have any professional (or even enthusiast) A/V equipment.
1. I don't have any media editing experience other than reducing the resolution
   of videos so that my colleagues and I can easily download them on our very
   slow connections.
1. The conference was organized very quickly without a lot of time to prepare.

## The Setup

The room's setup was pretty basic, considering we didn't really have many decisions
to make:
- Which smartphone can we spare to use it as the video camera?
- Can we record audio by connecting the conference microphone directly to a smartphone?
- Is the Director's projector available?
- Which of our external speakers is the loudest?
- How are we going to power everything? What if the electricity cuts out?

But we managed to use an android phone as our video camera, mounted on a tripod
in the middle of the room, just in front of a support column. I was able to test
the day before that at 1080p resolution, the phone had enough capacity to store
at least 6 hours' worth of video--well over the 4 or so hours that the conference
was expected to last. It would, however, need a power source beyond the built-in
battery.
![phone and tripod with battery]()

The office's USB conference mic *did* connect to another android phone, via a USB-microUSB
adapter, and I was able to confirm that this phone's storage and battery could
easily handle 6 hours' worth of audio.
![conference mic and phone]()

We added in a small projector and an external speaker, and we were surprised to learn
that there was a small PA system also available. My wife's laptop served the slides
and videos.

## The Execution

Amazingly, our electricity never cut out once during the conference. That was a
relief. The only real weak spot in terms of the in-person experience was that
the wireless mics cut out fairly frequently, probably due to them being very
sensitive to even small obstructions between them and the loudspeaker.

For some reason, during the first session the video camera quit recording an hour
in. I manually restarted it, but we'd already lost nearly an hour's worth of
footage before I found out.

Later on, during a Q&A phase of the conference, my wife decided ad-hoc to use her
phone as a camera so that she could pan it around to follow all the back-and-forth
discussion. That added a nice 2nd video feed to that part of the day.

## Post-Production

I spent a day learning how to use [kdenlive](https://kdenlive.org/en/) and working
with the day's media content. The software, like its analogs for photo editing
and audio editing, is a little opaque at first. There are so many boxes and windows,
and it's not clear at first what they're all for. But the online helps came in
handy, and I was soon getting all the content imported in.
![screenshot of content]()
