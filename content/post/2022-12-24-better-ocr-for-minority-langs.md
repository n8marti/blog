---
title: "Improving OCR for Minority Languages"
Date: 2022-12-24
tags: ["cli", "ocr", "open source", "machine learning", "cloud-server", "script"]
draft: false
---

After recently resorting to OCR software to salvage PDF content (more about that
in my [previous post](post/2022-11-12-lost-the-original)) I set off on an effort to improve
OCR effectiveness for African languages that use Latin-based writing systems.
<!--more-->

### The starting point

**Tesseract** is an open source OCR (Optical Character Recognition) program that
can be used on its own on the command line, but it's also used as a back end for
several [front end apps](https://tesseract-ocr.github.io/tessdoc/User-Projects-%E2%80%93-3rdParty.html).
In addition to being open source, another advantage is that, while OCR software
typically requires the user to choose an input language to help guide the
recognition process, **Tesseract** also crucially accepts various *writing scripts*
as input languages. This allows it to be used on minority languages for which
there does not exist enough published text nor user base to produce their own
specific language models.

In the PDF recovery project mentioned above I used the script option "Latin"
in order to scrape Banda-Linda text from the document because the Banda-Linda
language uses a Latin-based script. However, the Latin script model has some
shortcomings, namely:
- no recognition of characters rarely used in European languages, such as ɓ, ɗ, ɛ, ə, ŋ, ɔ
- poor recognition of combined characters that use diacritics rarely seen in European languages

I wanted a solution that would reduce or eliminate these shortcomings and work
at about a 98% accuracy rate. So I set about learning how to produce a new model
called "Latin_afr" by modifying the "Latin" model.

### The ingredients

I'll gloss over all the forum reading I had to do to figure out where to start,
but ultimately these are the ingredients I needed to make this happen:
1. a host system to do the processing, aka "training"
1. a way to generate synthetic data that included images of text and the corresponding text itself
1. up-to-date tesseract and tesstrain software
1. a way to reliably and repeatedly run the training process as different variables were tweaked
1. an objective measurement to quantify the effectiveness of each provisional model
1. a way to compare actual results with expected results in order to calculate the effectiveness measurement, including sample documents
1. a good old notebook-and-spreasheet combo for keeping track of all the details and comparing different provisional models

There's a lot going there, but suffice it to say that for the host system I took
advantage of a [DigitalOcean](https://www.digitalocean.com/) promotion and rented
a cloud server to do all the work. That way I wouldn't have to tie up my laptop
for hours (or potentially even *days*) at a time. I cloned and compiled the [tesseract](https://github.com/tesseract-ocr/tesseract/) and [tesstrain](https://github.com/tesseract-ocr/tesstrain/)
software from their respective GitHub repositories, and I wrote 3 scripts: one
each to handle synthetic data generation, running the training process, and comparing each
provisional model's results with the expected results. All of the necessary pieces
are housed in a GitHub repo: [https://github.com/sil-car/ocr](https://github.com/sil-car/ocr).

Probably the most interesting piece I hadn't considered very deeply before was
how to objectively measure the effectiveness of each model's OCR output.

### Measuring OCR effectiveness

It turns out that many other people have pondered how to quantify OCR effectiveness
for quite some time. There are two main variants of the character-based error rate.
For simplicity of calculation I chose the older, original **Character Error Rate**,
or CER, based on info I learned from these sources:
- http://www.dlib.org/dlib/march09/holley/03holley.html
- https://towardsdatascience.com/evaluating-ocr-output-quality-with-character-error-rate-cer-and-word-error-rate-wer-853175297510

The CER requires these 4 quantities:
1. N: number of characters in source text
1. S: number of substitution errors (i.e. wrong character was recognized)
1. D: number of deletion errors (i.e. character was left out of results)
1. I: number of insertion errors (i.e. character was added to results)

It is calculated as follows:
```
CER = (S + D + I) / N
```

I didn't bother with using a word-based error rate (WER), because that is most
useful for language-specific OCR models where you can provide a wordlist to help
improve the OCR results.

Armed with an objective measurement, I was able to test the various provisional
models and compare them with each other and the original Latin model. I was also
able to see if all the effort was going to help me reach my goal by seeing if
I could produce a model that was at least 98% effective (i.e. CER <= 2%).

### The steps

My training workflow basically looked like this:
1. Clean up from any previous trainings.
1. Generate synthetic data (images of text + matching text files) using https://github.com/sil-car/ocr/scripts/generate-training-data.py:
   ```bash
   $ # generate 64000 image/text pairs
   $ ~/ocr/scripts/generate-training-data.py -i 64000
   ```
   ![synthetic data example](/img/image-text-data.png)
1. Wait anywhere from 1 to 5 hours depending on how many images I decided to generate. This sometimes went as high as 100,000 images of 50 characters each.
1. Start the training using https://github.com/sil-car/ocr/scripts/run-training.sh:
   ```bash
   $ # replace starting "Latin" model's top layer, then run 58000 training iterations
   $ ~/ocr/scripts/run-training.sh -l Lfx512 -i 58000
   ```
1. Wait up to 13 hours depending on how many training "iterations" were requested.
1. Copy the resulting provisional model down to my laptop and use it to OCR excerpts from 15 different PDF documents that all come from central Africa and all use a Latin-based writing script. Count the substitutions, deletions, and insertions for each output text. This often took an hour for early, more poorly-performing models, down to about 20 minutes for the best-performing models.
   ```bash
   $ # use the model created on 12-19-2022 to OCR the sample image from Mandja
   $ ~/ocr/scripts/test-ocr.sh -l Latin_afr_20221219 data/example-documents/mza_mandja
   ```
   The above script opens this Meld window:
   ![mandja ocr test](/img/mza-test-ocr.png)
   The comparison data is entered into a spreadsheet:
   ![mandja data entry](/img/mza-data-entry.png)
1. Compare the model's CER with others and consider ways to make improvements.
   ![mandja data summary](/img/mza-data-summary.png)
1. Make any changes to the generate-training or run-training scripts, then go again.

All in all, I've created nearly 30 models by tweaking various details, such as
the number of fonts used in image generation, the relative proportions of the
various characters generated in each image, the number of images generated,
input image height, the type of training performed, and the number if training
iterations run (i.e. how many times the training program looked at input data and
tweaked the model). This process has gone on for 3 weeks. Often I've started a
round of training in the evening so that it could run through the night, then I
would evaluate the model early the next morning. That would allow me to make time
during the day to prepare to start the next iteration that evening.

### The result

This has been a *very abbreviated* description of this interesting and intense
project. The question that begs to be asked at this end of all the effort is,
"Was it worth it?" And the answer to that question is "...Maybe."

The best Latin_afr model I've produced so far does better than 5% CER on 11 of
the 15 test samples. It does better than my original goal of 2% CER on 7 of
those samples. While there might be some room for improvement in the model training
process, the poorest-performing samples have their own issues, such as "noise"
(splotchiness of the original piece of paper that was scanned) or lack of
sharpness in the scanned image, "old-school" fonts that I wasn't able to use in
the training, and very tight line spacing that overlaps diacritics from a lower
line with the hanging parts of characters from an upper line. Two of the poorest-performing
images did better with the original Latin model, so that could always be a fall-back
option as well, while the other two poor performers have a CER in the 7% range, which
could still be manageable. It's unclear whether or not a broader range of testing
will show different results.

But, as a next step, I've asked for feedback from various colleagues who might benefit
from this capability. Maybe I'll make another blog post down the road after that
feedback comes in.
