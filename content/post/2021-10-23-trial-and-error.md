---
title: Trial and Error
subtitle: Sometimes the "error" part wins
Date: 2021-10-23
tags: ["python", "script", "fail", "GUI", "LibreOffice", "open source"]
draft: false
---

Usually the phrase "trial and error" means you are persistent, and ultimately
successful, in an effort to solve a particular problem, even if your method
maybe isn't the most efficient one out there. But I think this is an apt phrase
for a recent *unsuccessful* effort I undertook with the goal of splitting a
multi-language ODT file into separate text files for each language.
<!--more-->

I was helping some colleagues split a LibreOffice ODT file apart according to
each paragraph's language. The text needed end up as two plain text files, one
for each language, so that it could be imported into another program. The
language of each paragraph more or less alternated,
but it was not guaranteed to be a clean "A B A B" pattern throughout the document,
where I could have simply sent even paragraphs to one file and odd ones to another.
The document was 900+ pages long, so no one was going to highlight each language's paragraph one at a
time to copy/paste them into a text file. Being eager to help solve the problem, I
dove right into the *obvious* solution of writing a [Python](https://www.python.org)
script to find out the language of each paragraph from the underlying XML and
copy it accordingly to the appropriate text outfile.

Fast forward past a little bit of research on odfpy's [GitHub page](https://github.com/eea/odfpy/wiki)
and some broader searching on [DuckDuckGo](https://duckduckgo.com), and I had a
functional but ineffective script. It was not properly determining the language of
a significant percentage of paragraphs. Fast forward another couple of hours, while
multitasking on other work and trying to figure out how to deal with tracked
changes and comments, I realized that I had made good progress, but it
was clear that my script was not going to solve the problem very effectively.

![](/img/GitHub-filter-lg-script.png)

So, I did what any wiser person would have done from the beginning and started searching for help
with the actual problem I had rather than with the solution I had hastily
decided on (classic [XY Problem](https://xyproblem.info)). And what do you know? It
turns out you can actually use LibreOffice's Find & Replace window to *search by language*!
It's buried behind the "Format..." button under the "Font" tab, but it's there.

![](/img/LO-If-Kipling.png)

And voilà! I was able to use a few keyboard shortcuts and mouseclicks to accomplish  
in **3 minutes** what I had failed to effectively accomplish with a Python script after  
nearly **3 hours**! Better to invest a little bit of time to research your options
before deciding on a course of action in order to save even more time on the execution.
