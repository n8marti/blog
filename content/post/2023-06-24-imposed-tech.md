---
title: 'Imposed Tech'
Date: 2023-06-24
tags: ["libreoffice", "open-source", "sustainability"]
draft: false
---

Here in Africa people who use technology are suffering through a kind of forced assimilation into a Western paradigm.
<!--more-->

In many ways Africa has no choice but to try to fit into the technological world created and advanced by the West.
Development is further behind in Africa, so people here are always playing catch-up Western developments and innovations. They have no choice but to try to assimilate, otherwise they risk getting more and more marginalized. But in one specific aspect, Africans have a choice and shouldn't feel forced to assimilate: **They can choose to forego licensed software when comparable non-licensed software exists.** And, likewise, **Westerners can choose to support that effort by conciously adapting our workflows to accommodate non-licensed software.**

The status quo is *unsustainable* because it requires a significant amount of effort manage licensed software. This effort is simply not required for non-licensed software. Secondly, much of this effort is carried or supported by Westerners, which makes our African colleagues ultimately *dependent on us* in yet another way in order for them to carry out their work.

### The imposition of licensed software

Licensed software requires at least some, and usually all, of these things:
1. Money.
1. Internet access to register the product and/or receive the license code.
1. An email account to register with the software vendor.
1. An online payment method with backing from an internet-compatible bank account.
1. An ability to keep track of essential relevant information: license code, expiration date, associated email account.

A recent article by [The Hustle](https://thehustle.co/about/), ["Why so much of the world runs on counterfeit software"](https://thehustle.co/why-so-much-of-the-world-runs-on-counterfeit-software), highlights some of the difficulties with these impositions for users in Nigeria.

Let's compare these requirements with the means of someone in the developing world.
1. Money: People (and organizations) have much less disposable income than Westerners.
1. Internet: The connection is slow, intermittent, expensive, and reliant on electricity, which itself is unreliable.
1. Email: Many people have accounts. In fact, most people I know have one account they use now, and two or three older
ones for which they forgot the passwords, so they simply ditched them rather than tried to recover them.
1. Online payment: Almost nobody has a credit or debit card (even local *organizations*), let alone one that is accepted for internet payments.
1. Information management: People are not accustomed to storing digital information in a way that they can be sure to find it again a year from now. Email accounts get ditched, hard drives fail, backups are not even a consideration--even when files are retained they can be hard to track down again if a person hasn't thought through how to organize their files.

### Trying to bridge the gap

As an IT Administrator, I spend a large part of my time compensating for these differences between these imposed expectations and the local realities: 
1. Money: I have to communicate with at least two different people to determine if there are funds available for a particular license for a particular user. The answer is just as often "no" as it is "yes".
1. Internet: I have spent *hundreds* of hours trying to increase our office's internet capacity while also trying to make more efficient use of what we do have.
1. Email: Multiple times per year I'm asked by users to help recover online accounts of various kinds. Sometimes I'm successful.
1. Online payment: I regularly rely on license payments being carried out indirectly through inter-organizational accounts (managed by Westerners) rather than directly through a bank account. This requires significant time communicating with both ends of the transaction to clarify account details, procedural steps, etc.
1. Information management: I maintain multiple spreadsheets of users' license information for various software.

In the West I would wager that the *user* handles all of this without even thinking twice, because these are commonplace requirements. *There* the IT Administrator is free to spend more time solving unique problems and less time managing these "basic" requirements. *Here* many unique or challenging problems have to wait to be solved because the IT Admin's time is limited and is taken up with the above tasks.

**This is an unacceptable inefficiency** that can and should be mitigated. Otherwise, how can I train African colleagues to perform the more advanced tasks in IT Administration if *their* time largely taken up, as well, with trying to compensate for the above impositions?

**This is an unacceptable dependency** on Westerners. The reality is that Westerners such as myself are involved to help manage the above process at every step. We are perpetuating *yet another* form of dependency on our African colleagues.

### Another [better] way

Non-licensed software (which often happens to also be open-source software) is an obvious path out of this quagmire. As soon as you drop licenses, you no longer need to have money, internet, email, online payment, or information management in order to use the software. Genius! Sure, free software often has its own limitations: it's sometimes less polished or has fewer features, it sometimes has less direct means of support, and it sometimes comes with hidden costs (the biggest one being the sharing of user data for marketing purposes, à la Meta [Facebook/WhatsApp/Instagram], Amazon, Netflix, Google, etc.).

But this is not necessarily the case. Many non-licensed, open-source projects include as many or more features as their licensed counterparts: [LibreOffice](https://www.libreoffice.org/), [Firefox](https://www.mozilla.org/en-US/firefox/new/), [Thunderbird](https://www.thunderbird.net/), [VLC](https://www.videolan.org/vlc/), [Audacity](https://www.audacityteam.org), [Ubuntu](https://ubuntu.com/download/desktop), [GIMP](https://www.gimp.org/), [DaVinci Resolve](https://www.blackmagicdesign.com/products/davinciresolve/), [Blender](https://www.blender.org/), [Scribus](https://www.scribus.net/). Most licensed software today only provides support in the form of web forums, which is what non-licensed software has been doing for years. And frequently, if you want more direct support, in both cases you can pay for a support package. Finally, most _open-source_ projects explicitly _do not_ require user information, neither directly through product registration nor indirectly through usage tracking, to function normally.

### So why do we do it the hard way?

Inertia. The West is comfortable using Microsoft products wherever they exist because they offer the path of least resistance. MS Office is often pre-installed when you buy a PC, or there's a pre-installed, free trial version that allows a user to buy a license with just a few clicks (because, remember, users in the West have money, internet, email, credit cards, and a way of managing files that they are accustomed to). We're also comfortable with buying the latest Adobe product for our publication needs because we know it works and we know others who use it and can help us if we have questions. And we pay for antivirus licenses because our licensed *operating systems* are prone to attack, and we can't afford to lose time trying to recover from such attacks. Changing these well-established norms would require significant time invested in educating and training people on other options. So the tradeoff here is that we inadvertently require people (especially IT Admins) in Africa to spend their time trying to "bridge the gap" so that we can save ourselves the time of having to adapt to something different. *They* should adapt to *us*!

Secondly, the West has long adapted--or maybe "resigned itself" is a better phrase--to using licensed software. This paradigm started logically with developers creating a product that cost them time and energy, and so they merited compensation from those who benefited from their work. After all, we buy things all the time that others produce--it's basic economics and practically a law of the universe.

But what we users have failed to realize, and what software sellers are now capitalizing on, is that once the product is developed it costs literally *nothing* to reproduce it, whether that's 1 or 1 billion copies. Yet we still pay for each copy... However, a growing paradigm espoused by the open-source world is one where vendors distribute their product for *free*, then provide additional services for those willing (and able) to pay. This the business model behind LibreOffice, Ubuntu, and many others.

### The way forward

I think three things have to happen to overcome this incompatibility between existing Western expectations imposed by licensed software and the unsustainability in Africa of trying to meet those expectations:
1. *Westerners* need to be educated about the challenges that these impositions pose.
1. *Africans* need to be educated about software alternatives that don't come with such impositions.
1. Both *Westerners and Africans* need support for adapting existing workflows in order to incorporate new software.

I'm ready to do what I can on all three fronts. I hope I can find many others to come alongside to help me.
