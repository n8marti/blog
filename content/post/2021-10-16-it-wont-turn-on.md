---
title: It Won't Turn On
#subtitle: Subtitle
Date: 2021-10-16
tags: ["desktop support", "hard drive"]
draft: false
---

An administrative-level colleague summoned me to his office one day to ask for
help because his computer wouldn't turn on. The cause turned out to be rather
surprising.
<!--more-->

I often get very generic descriptions like this that could mean several things:
- "there's no response from my computer when I push the power button to turn it on"
- "all I see is a black screen with a small cursor when I turn on my computer"
- "there is no input shown when I try to type my login password"

So we start at the beginning so I can see for myself what is or isn't happening,
as well as find out what the user is expecting to happen differently, if it's
not clear to me.

So then my colleague powered on his laptop. This is what we saw:

Stuck just after BIOS screen|
-|
![image: black screen, white cursor](/img/black-screen-white-cursor.jpg)|

So I brought it back with me to my office and booted it with a live USB: No sign
of the hard drive listed among the available devices. So then I popped off the back
cover, suspecting a loose connection, and, lo and behold, there *was* a
connection problem, of a sort: *the hard drive wasn't connected because **it wasn't there at all!***

Bewildered, I returned to my colleague's office and asked him if he had any idea
about why his hard drive might have been removed. He said hadn't used it since
the previous Friday (it was now Tuesday), and it hadn't left his office.
>Me: Are you sure you didn't get the laptop repaired over the weekend?  
Him: I'm sure. It stayed here in my office.

>Me: Are you sure you didn't take it home, and maybe some enterprising young
family member took it out for who-knows-what-reason?  
Him: Nope. It never left my office.

I was all out of ideas. We both threw up our hands, and I went about getting
a new hard drive for him, reinstalling the OS, and setting up all his software
anew.

We never did find out what happened to that hard drive, but it sure makes for an
interesting story to tell!
