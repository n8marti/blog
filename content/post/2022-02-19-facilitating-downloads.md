---
title: Facilitating Downloads
Date: 2022-02-19
tags: ["download", "cloud server", "rsync", "wget", "open source", "CLI"]
draft: false
---

Downloading big files is hard when the connection is both slow and intermittent.
This has caused me to get creative sometimes when trying to download things like
videos, ISO files, or large software installers.

<!--more-->

## The Problem: Frustrated by a Limited Connection
Because of the limited connection, our office's network is heavily managed by a [pfSense](https://www.pfsense.org/)
firewall so that certain kinds of traffic get priority (email, training sites,
work-related public servers, etc.), while others (e.g. general http/s traffic,
which includes all downloads) take a back seat. Each individual user also has a
hard limit to keep any one person from "hogging" all the bandwidth.

This is great for making sure all the important things can get done during the
work day, but it's terrible for getting big files downloaded. Sure,
I can log into the network during off hours to do big downloads,
but even if the bandwidth is better, sometimes the connection is still
intermittent, and downloads can still fail or time out or otherwise not succeed.

## The Solution: Cloud Server to the Rescue!
So, one of my "tricks" has been to use a cloud server as an intermediary. This
accomplishes 2 things:
1. I can download a large file that has a time-sensitive link (*ahem*, Win10_21H2_French.iso) before the link expires.
1. I can use the required http/s link for the initial download but use a more robust rsync connection to get it to the office network.

### The Setup
I rent a [DigitalOcean](https://www.digitalocean.com/) server that serves a handful of functions. One of them is
to run an rsync daemon, which lets me make certain folders accessible using the
rsync protocol over the server's public IP. Let's pretend the IP is **142.250.186.46**.
(This is actually one of Google's public IPs. I'm avoiding sharing my own to avoid
the possibility that the server gets spammed by one of the zillions of readers of
this blog. ;-) )
To set this up, do the following:
- Log into your cloud server's CLI. I use SSH; you could also use the browser terminal from the provider's server management page.
  *Note: I don't actually have the SSH port open on the public IP. Instead, I use a wireguard VPN connection and ssh to it, which is outside of the scope of this post.*
  ```bash
  laptop:~$ ssh user@142.250.186.46
  Last login: Mon Feb 14 16:22:29 2022 from XXX.XXX.XXX.XXX
  server:~$
  ```
- Make sure your server's firewall allows incoming connections on the rsync port (default=873).
  How to do this depends on your hosting platform.

- Update packages list and install rsync (my server runs Ubuntu).
  ```bash
  server:~$ sudo apt update && sudo apt install rsync
  ```
- Create your share folder.
  ```bash
  server:~$ sudo mkdir -p /srv/rsync/share
  ```
- Configure the rsyncd config file; make the share read-only and limit the number of simultaneous connections.
  ```bash
  server:~$ sudo nano /etc/rsyncd.conf
  ```
  ```
  [share]
    path = /srv/rsync/share
    comment = shared files
    read only = true
    max connections = 5
  ```
- Enable and start the rsync systemd service.
  ```bash
  server:~$ sudo systemctl enable --now rsync.service
  ```
- Open a 2nd terminal and test your rsync service from your laptop. Install rsync first, if you need to.
  ```bash
  laptop:~$ sudo apt update && sudo apt install rsync # if needed
  laptop:~$ rsync rsync://142.250.186.46
  share           shared files
  ```

### The Execution
- Go back to your server terminal and download a giant file to your cloud server.
  ```bash
  server:~$ cd /srv/rsync/share
  server:/srv/rsync/share$ sudo wget https://releases.ubuntu.com/focal/ubuntu-20.04.3-desktop-amd64.iso
  [sudo] password for user:
  --2022-02-14 17:27:04--  https://releases.ubuntu.com/focal/ubuntu-20.04.3-desktop-amd64.iso
  Resolving releases.ubuntu.com (releases.ubuntu.com)... 2001:67c:1360:8001::33, 2001:67c:1360:8001::34, 2001:67c:1562::25, ...
  Connecting to releases.ubuntu.com (releases.ubuntu.com)|2001:67c:1360:8001::33|:443... connected.
  HTTP request sent, awaiting response... 200 OK
  Length: 3071934464 (2.9G) [application/x-iso9660-image]
  Saving to: 'ubuntu-20.04.3-desktop-amd64.iso'

  ubuntu-20.04.3-desk 100%[===================>]   2.86G   174MB/s    in 17s     

  2022-02-14 17:27:21 (175 MB/s) - 'ubuntu-20.04.3-desktop-amd64.iso' saved [3071934464/3071934464]
  ```
  Wow, 17 seconds! That would have taken *hours* on the office connection, and,
  depending on the way the file is made available, it could be a challenge to
  continue the download where it left off if it fails.
- Switch to your local terminal and get the file from your server with rsync.
  ```bash
  # Make sure the file is there first.
  laptop:~$ rsync rsync://142.250.186.46/share
  drwxr-xr-x          4,096 2022/02/14 17:27:04 .
  -rw-r--r--  3,071,934,464 2021/08/19 12:06:00 ubuntu-20.04.3-desktop-amd64.iso
  # Now bring it down; restart at any time with the same command
  #   if the connection breaks.
  laptop:~$ rsync -Pz rsync://142.250.186.46/share/ubuntu-20.04.3-desktop-amd64.iso .
  ubuntu-20.04.3-desktop-amd64.iso
        688,128   0%  282.23kB/s    3:01:21
  ```
  I use -P and -z options to keep partial files and to show download progress (-P), and to compress the data during transfer (-z).
  I canceled my download because I didn't actually need the file, nor did I want to wait 3 hours for a file that downloaded to my cloud server in 17 seconds!

There you go! There's one way to use a cloud server to avoid complications when
downloading a large file on a slow and intermittent connection.
