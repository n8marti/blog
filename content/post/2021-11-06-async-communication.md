---
title: Efforts at Leading a Team Asynchronously
Date: 2021-11-06
tags: ["remote teams"]
draft: false
---

I've been leading a dispersed committee of 8 people for several months, and we've
found it challenging to move our work forward for multiple reasons, including:
1. We're all very busy with our "day jobs" and have a hard time squeezing in committee-related tasks.
1. I personally traveled for several months and temporarily had new, additional
responsibilities that required my near-full attention.
1. The internet connection we use in central Africa has significantly degraded
to the point where audio and video calls are only feasible during the network's
off-peak hours (i.e. before 9am and after 6pm).
<!--more-->

This has forced me to rethink how we as a committee try to accomplish our goals.
It was obvious to me that live audio/video calls are not a reliable means to move things
forward, but what alternatives were there? What kind of mental framework could be
used as a foundation for pursuing alternatives and deciding if they were effective
or not? So I started looking for wisdom that others have already gleaned from the
pandemic's effect of forcing them to work remotely, and that's when I learned
about *asynchronous communication*, as defined by GitLab, a platform for hosting
shared software code and [other content](https://about.gitlab.com/what-is-gitlab/)
related to software development.

### Async Defined
GitLab's [definition](https://about.gitlab.com/company/culture/all-remote/asynchronous/#what-does-asynchronous-mean):
*Asynchronous (async) communication is the art of communicating and moving
projects forward without the need for additional stakeholders to be available at
the same time your communique is sent.*

### Async in Practice
For the past month I've intentionally focused on using platforms other than video
calls to introduce new committee tasks, assign task ownership, and generate
documentation. The default platform for most people in our organization is email,
so I still rely heavily on it. But I've also complemented it with group chat,
cloud-based file sharing, and task sharing (all within the Google ecosystem), as
well as messaging app communication outside of Google's offerings.

#### The benefits I've seen so far are:
- Tasks can be completed when committee members are able to make time for them.
- More members are able to give input when making decisions.
- Progress is made is smaller steps, but these steps are accomplished more frequently.

#### And some limitations I'm still trying to work out are:
- It's harder for the team to feel cohesive without synchronous communication.
- Some members come from cultures that prefer oral, in-person (i.e. synchronous)
communication and find it hard to prioritize communication that happens
asynchronously.
- Some members are less confident in relying on written communication skills (i.e.
typing and careful analytical reading).

I'm hoping to apply what I've learned here to collaborative work I occasionally
perform with other colleagues. We'll see how it goes.
