---
title: "Distracted Destruction"
Date: 2023-03-18
tags: ["cli", "fail", "recovery", "virus"]
draft: false
---

A whole day can be ruined in just 3 seconds!
<!--more-->

It's common for me to be helping someone with a computer problem, and they just
decide to stay and hang out with me while I work. It's nice to chat with people,
hear how their work is going, how their family is doing, etc. But it can also be
risky, depending on the task I'm working on.

### Infected USB Flash Drive

A user came to me this week with a relatively common USB flash drive problem:
All the files were gone and replaced with a single EXE file called "USBDriver". And,
contrary to what you might be led to believe, the user did *not* have a driver
problem with his USB ports, nor was double-clicking on "USBDriver" the solution.

This is a virus that I see pretty frequently around here. Maybe I'll post more
about it in the near future, but for now I'll just say that when the virus
infects a USB flash drive, all the existing contents are hidden away, and this single
EXE file is placed at the top level, just begging you to double-click it to "fix"
the USB drive.

I was able to use a simple terminal command, [**find**](https://manpages.ubuntu.com/manpages/focal/en/man1/find.1.html) to move all the files to a safe place so I could format the drive
and put them back. Then came the formatting step. I use the **Disks** app, which
has a no-nonsense interface and provides several useful disk-related operations.

![important disk](/img/disk-important.png)

So, while chatting away with my office guest, I select the disk and let muscle
memory take over. I choose to reformat it as FAT32, which I've done dozens of
times, I give it the same name it had before, wait 3 seconds, and voila, done!
*Except*, that after I was done, I realized that I now had two disks listed with
the same name. *Rather than reformatting the USB drive, I had actually reformatted my SD card!*

![reformatted SD card](/img/disk-reformatted.png)

I just dropped my head into my hands at that point. That SD card is my extended
storage for my system. The picture above is of an alternate one of 64 GB that I used for
this post, but the real one holds 400 GB, and it was nearly full at the time.
And it was all gone in 3 seconds.

### Recovery?

After a few seconds of pure demoralization the problem-solving side of my brain
kicked into gear. I know of useful tool out there for recovering files and partitions
called [**TestDisk**](https://www.cgsecurity.org/wiki/TestDisk_Step_By_Step). It's
saved the day for me a few times in the past, so I was ready for it to save me
again.

Every kind of storage disk, no matter what format it uses, has a similar basic
layout: the beginning part of the partition (where the data is actually stored)
contains information about what files are in the partition and where they are
found. My SD card had been formatted with an ExFAT partition, and it had such a
table of information at the beginning, as well as a backup copy in case the first
one is corrupted. **TestDisk** has a tool for restoring a partition table from
its backup. And, *crucially*, basic formatting of a storage disk really just
means rewriting the beginning part of the partition with new information. So this
is why tools like **TestDisk** are able to undelete files that you think are
completely gone. Info about those files has been deleted from the partition table
(or, in my case, the partition table itself has been deleted), but the actual files
are still somewhere on the disk but orphaned from the addressing system used to
find them. There was hope!

*Unfortunately*, after searching, **TestDisk** wasn't able to locate the backup
partition table. It also wasn't able to rebuild it (not quite sure how that works,
but it was worth a try). So now I've resorted to using a sister app to **TestDisk**,
called [**PhotoRec**](https://www.cgsecurity.org/wiki/PhotoRec),
which can recover files based on known file types. This can
be very useful, but it recovers files based solely on recognizable data *within* a
file, which means it can't recover file names. Not a big deal if you're recovering
a bunch of photos that were just named by date & time, but more of a problem if
you have, say, a dozen or videos that only differ from each other by the language
of the audio track used. Or if you have several Windows installer ISO files that
are all about the same size. *Sigh*. Better than nothing.

I also have an old backup somewhere of this disk, and since its contents don't
change a lot over time, I'm hoping that will also get back most of what I lost.
We'll see.

### Parting Thoughts

But what is the lesson in all this? It's obviously risky to do destructive work
while being distracted. It's also hard to repeatedly tell people that you need
silence and/or to be alone when doing such tasks if the task is only going to
take a few seconds to complete. But then again, ***a lot can happen in those 3 seconds!***
