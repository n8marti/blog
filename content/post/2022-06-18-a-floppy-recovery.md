---
title: A Floppy Recovery
Date: 2022-06-18
tags: ["backup", "recovery", "old-tech"]
draft: false
---

What do you do with old floppy disks that may or may not have corporate data on them?
<!--more-->

I've been trying to clean out old cruft from my IT office, and I decided it was time
to deal with the 300ish floppy disks that have been in storage there since the
1990s (I was in elementary school at the time, in case you were wondering). They've
been there so long that no one currently in our organization knew anything about
them, or even that they existed.

![my old friends](/img/floppies.jpg)

## Accessing the disks

After failing to resurrect an old PC (see [Floppy Disk Reader Fail](/post/2021-11-27-half-the-day-floppy)
for more on that), I decided to order a USB floppy disk reader and wait until
someone could hand-carry it to me from the US. Then, about a month ago, I dove
back into this project.

![my new friend](/img/floppy-reader.jpg)

## Sorting through the pile

Now, I wasn't about to try to copy *all* the files off of *all* the disks without
some kind of triage step. Thankfully, I could set aside nearly half of them that
were obviously old software installers like these (by the way, that's the remnants
of a rubber band around the MS-DOS disks):

![software](/img/floppy-software.jpg)

A few of the disks were personal communications from back in the day that used an
email service called [cc:Mail](https://en.wikipedia.org/wiki/Cc:Mail). I put them
aside, while I wait to see if anyone who was around at that time might like to keep them
as mementos. A few more disks clearly contained original documents with information
that was destined at some point for publication. These I set aside for later
discussion with the relevant members of our team.

Another category, which was nearly the rest of the lot, was some kind of collection
of meticulously-named disks with names like "FCXX#.COR 1". After some asking around,
it turns out that these contain government census data from a particular year.
There's quite a story behind that, but I won't go into it here. At any rate, I
decided to preserve the contents of all 150 of these, since the government *might*
someday decide to ask about them.

## Imaging the disks

So, what's the best way to preserve floppy disk contents? I don't know, but I
quickly found out that many/most of the disks were in poor shape and couldn't be
directly read by my system. That meant that simply opening each disk then
copy/pasting its contents somewhere wasn't going to be reliable. So, after some
quick research, I decided to use [**ddrescue**](https://www.gnu.org/software/ddrescue/)
as a best-effort method of imaging each disk. Then, later, the disk images could
be used to recover the files themselves.

There ended up being about 160 disks that I decided were worth imaging. The ones
in good shape could be imaged in just under a minute. (Apparently, even new floppy
disk readers can only read a floppy disk at a snail's pace.) Here's a sample of
what **ddrescue** looks like in action:
```
$ sudo ddrescue -d /dev/sda disk.img disk.log
11:37:58 Block copy size: 128
GNU ddrescue 1.23
Press Ctrl-C to interrupt
     ipos:    1439 kB, non-trimmed:        0 B,  current rate:       0 B/s
     opos:    1439 kB, non-scraped:    34816 B,  average rate:     183 B/s
non-tried:        0 B,  bad-sector:   522240 B,    error rate:     102 B/s
  rescued:   917504 B,   bad areas:        2,        run time:  1h 23m 25s
pct rescued:   62.22%, read errors:     1029,  remaining time:         n/a
                              time since last successful read:  1h 22m 45s
Scraping failed blocks... (forwards)
```
I don't have a complete understanding of all the steps, but basically **ddrescue**
will make multiple attempts to read each of the disk's sectors, starting with a
"quick search" that moves quickly through the disk to get as much low-hanging fruit
as possible, followed by a "deeper search" that tries harder at each sector
but also risks damaging the disk further. Additional options can be added for
things like trying multiple times and using different sized blocks when attempting
to copy the data out. I chose to run the command multiple times, progressively cutting
the block copy size in half each time from 128 bytes to 64 bytes to eventually
1 byte, with the hope of getting as much data as possible by the end. And for each
run of the command, I chose to attempt to read each sector 3 times. I didn't
care if all the effort ultimately damaged the disk further, since I was just going
to toss them afterwards anyway.

#### So, where did all this effort get me?

Well, I was able to recover over 95% of the data on nearly all of the floppies.
That's great, considering probably half of them couldn't be read at all on the
first attempt. All in all, the total amount of data recovered is *at most* 230 MB
(1.44 MB per disk x 160 disks). That's crazy, considering how much time went into
this project. Which brings up the next question...

#### And how long did it take?

Well, I didn't keep track of all the hours, but I would estimate that the total
time spent on the project ended up being about 16-20 man-hours, plus another 40+
computer-hours where I did other things while my raspberry pi
[Ubuntu server](https://ubuntu.com/download/raspberry-pi) was running the script
to do all of the actual disk scraping. This was spread out over multiple weeks,
so it wasn't too mind-numbing, thankfully.

#### Is anyone ever going to look at these files ever again?

As my 7-year-old daughter likes to say, "No-eye deer!" But at least I can finally
get rid of the disks with a clean conscience.
