---
title: "Data Conversion with OCR's Help"
Date: 2023-04-29
tags: ["cli", "data-conversion", "ocr", "open-source", "recovery", "script"]
draft: false
---

A high-quality image of an old document is useless if you want to revise the text.
<!--more-->

### The problem

I've mentioned in two previous posts how I am sometimes [asked to recover text](/post/2022-11-12-lost-the-original)
from old documents, a task which has been made easier with the specialy-tailored
OCR model [that I created](/post/2022-12-24-better-ocr-for-minority-langs) called *Latin_afr*.
More recently, I was asked to recover text from several books of the
Bible, including *The Gospel of Luke*, in a language called [Yaka](https://www.ethnologue.com/language/axk/).

Luke has 24 chapters, about 1,150 verses and around 35,000 words, depending on the language in question.
Those who originally published the text many years ago would now like to use it
to aid in translating additional books of the Bible. However, the only copies that
still exist are printed versions and an archived TIFF document; i.e., the orignal
[**Paratext**](https://paratext.org) files are lost. There isn't even some
other kind of text-editable form that can be found, whether that be a generated PDF,
word processor file, etc.
![Cover page](/img/yaka-luke-image.png)
*Cover page of the TIFF image document. Being an image, the text is not selectable.*

### The workflow

So my task was to take this TIFF image-based document and produce a text-based file
that includes specific, inline format markers called
[Standard Format Markers](https://ubsicap.github.io/usfm/about/index.html)
to manage styles and formatting. This is the format that **Paratext** expects. Standard
Format Markers look like this:
![SFM sample](/img/sfm-sample.png)
*Screenshot from https://ubsicap.github.io/usfm/chapters_verses/*

There's no such thing as a point-and-click kind of tool for this conversion, so I
needed to piece together my own steps to make it happen. Here's the outline, with the
details following further below:
1. Prepare document for conversion
1. Convert image pages to plain text
1. Clean up the plain text
1. Convert the plain text to SFM text
1. Clean up the SFM text

### Prepare document for conversion

I was starting with a TIFF document (i.e. one TIFF image file containing multiple pages),
but my OCR software only recognizes the first page of any TIFF file because it assumes it's
a simple *image* rather than a *document*. So I needed to first split the pages out into
their own single-page image files. Actually, I ended up finding it more useful to simply
convert it into a multi-page PDF. For this I used the **tiff2pdf** commandline utility.
This only took about 10 minutes to find it with a websearch (Ubuntu package named *libtiff-tools*),
install it, and run it on my TIFF document. I now had a 126-page PDF that my OCR software
could read.
![PDF of The Gospel of Luke](/img/luke-pdf.png)
*Luke Chapter 1 in Yaka*

### Convert image pages to plain text

This is where the OCR process comes in. I have decided that
[**gImageReader**](https://github.com/manisandro/gImageReader) is the most user-friendly,
open-source tool for OCR work that uses
[**Tesseract**](https://tesseract-ocr.github.io/) as its OCR engine. If you'll remember,
**Tesseract** is the engine for which I build my *Latin_afr* language model.

However, before launching the OCR process there are a few steps worth taking that
can make the final result much more accurate:
1. The first is constraining the images to a suitable image resolution (I used 300 DPI)
if they start out higher than that.
2. The second is to tweak the brightness and contrast of each page to help minimize visual
noise. I decided to test various settings on a few pages, then apply those settings to *all*
the pages, rather than take the time to fiddle with custom settings for each of the 126 pages.
3. And probably the most helpful step was to develop a list of "blacklisted" characters that
the OCR engine would be instructed to discard as possible outputs. This was needed because
the strength of the *Latin_afr* model is also its weakness: because it can recognize thousands
of character combinations (lowercase, uppercase, top diacritics, bottom diacritics, etc.)
it can have trouble distinguishing between similar characters, especially diacritics. For example,
it might output "ó" instead of "ô" because they are sufficiently similar in appearance. So in
this case, adding the "acute" accent to the list of blacklisted characters avoids this problem,
assuming you *never* want it to recognize something as having an acute accent.
![blacklisted characters](/img/char-blacklist.png)
*Blacklisted characters*

This pre-OCR work took a little over an hour, but then I was ready for the main event. I
started the recognition process on all 126 pages at let it do its thing. It ran for about 30
min, and I finally had my plain text!

### Clean up the plain text

Unfortunately, this process is still not 100% accurate. Even if it were 99% accurate, I'd still
end up having to read through all 126 pages' worth of the plain text output and try to find
those 1% errors. So this is where most of my time was spent out of this whole process: comparing
the plain text side by side with the original images, trying to corrrect typos in a language I
have zero familiarity with.
![Comparing the original image with the recognized text](/img/compare-src-and-dest-text.png)

Many errors were repeated: an image of "â" often got recognized as "aâ", for example. So in many
cases I could use find and replace with [regex](https://regextutorial.org/) (basically a special
syntax to add high precision to searching and/or replacing text) to take care of lots of similar
errors all at once. Even still, it took more than 20 hours to finish this proofreading work.

When it was all over and done I had averaged about 24 words per minute to proofread the 35,000+
words of the document. I didn't test my speed at trying to simply type the text out directly by
looking at the images, but I suspect I would have gone at best half that speed, taking closer to
50 hours to complete. And then I still would have needed to proofread it after the initial typing.

### Convert the plain text to SFM text

SFM files have dozens of potential markers to choose from, but some of them are very predictable
and easier to automate. So part of my work in the previous step was to make line spacing consistent
between pages, paragraphs, headings, and chapter numbers. This made it fairly straightforward to
write a script that would replace the consistent line spacing with the relevant \p (start of paragraph),
\s (section heading), \c # (chapter numbers). Similarly, verse numbers are pretty predictable,
so my script was also able to add \v # (verse numbers) for each of them. Each of these 4 markers
must also be placed at the beginning of a line, so the script took care of putting all the line
breaks in the right places for this to be satisfied:
![Plain text at left, SFM text at right](/img/plain-vs-sfm.png)
*Plain text at left, SFM text at right*

Another fairly common set of markers that I didn't think would be worth trying to automate was those
used for footnotes. So I (and my son) ended up converting them manually. There were fewer than 20,
anyway, so it was no big deal.

Writing and executing the script for the conversion to SFM, along with manually converting
the footnotes, took less than an hour and a half.

### Clean up the SFM text

Final step! Since this project will continue in **Paratext**, I chose that venue for the final
cleanup. I did the work of creating the project template in **Paratext**, then I copied in the
SFM text I had come up with. **Paratext** has some very handy features to aid in basic proofreading
and consistency checking, so I took advantage of a couple of them at this point:
1. I had the app search for chapter and verse numbering errors, since it knows how many of each are supposed to exist in
this book.
1. And then I used a view of the text called "Standard", which uses the SFM markers to
display the text with styles and formatting added, and I compared the layout with the original
images. This helped find some section headers that had been mis-identified as new paragraphs, for
example. This final step took less than an hour, including exporting a typeset PDF, which I could
then send on to others as proof that I had completed the work.

This project actually encompassed 6 books, of which Luke was the largest. The whole project took
about 37 hours, and with all steps included the overall conversion rate was about **23 words per minute**.
Now the translation team can benefit from these earlier texts as they continue their work.
![Final product!](/img/yaka-luke-draft.png)