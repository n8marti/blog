---
title: Connection Troubleshooting
subtitle: How many ways can it be broken?
Date: 2021-10-30
tags: ["internet", "desktop support", "network"]
draft: false
#disqus_identifier:
#disqus_title:
#disqus_url:
---

I spend a lot of work time (read: about 10%) troubleshooting people's internet
connections, including my own. It's amazing how many ways an internet connection
can and does go wrong! Here is a list of *actual* causes that I've discovered,
not just hypothesized about (<mark>**most common**</mark>, **frequent**, less common, *rare*):
<!--more-->

- *public website down?*
- problem with the ISP's overall service availability?
- <mark>**3G network saturated?**</mark>
- modem reception problem?
- **modem auto-switched to 2G instead of 3G?**
- **modem firmware glitch?**
- too many devices connected to modem?
- <mark>**insufficient pay-as-you-go credit?**</mark>
- **SIM card no longer valid?**
- *misconfigured APN on device?*
- *local network routing problem?*
- **user's device not properly registered on the local network?**
- *user's network card malfunctioning?*
- *user's software malfunctioning?*
- user attempting to access restricted content?
