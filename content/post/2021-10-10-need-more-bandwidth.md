---
title: Need More Bandwidth!
subtitle: What it takes to get 6 Mbps download speed
date: 2021-10-10
tags: ["internet", "electricity"]
---

Working in the developing world is expensive. One example of that is having to
build in your own electrical infrastructure in order to provide power to an
already-expensive satellite internet connection... that only guarantees 6 Mbps
down...
<!--more-->

### 3G is the baseline

Where I work we have a single 3G internet connection that I have tried to make
work for a couple of years for about 40 simultaneous users. Needless to say, it
comes up woefully short in today's post-pandemic, zoom-every-meeting world.

### Satellite is the only way to grow

I recently started working to price out a top-of-the-line satellite connection
(no, Starlink isn't here yet), but the cost is staggering for the bandwidth you
get out of it. Let's just say it costs six figures to provide 1 year's worth of
satellite-based internet with a guaranteed download speed of about 6 Mbps. A
huge portion of that cost comes from the satellite equipment and monthly service
plan, but an additional non-trivial amount comes from the need to provide a
nearly 24/7 power backup system.

### But it needs a helping hand

Where I am electricity is neither constant (i.e. available 24 hours a day, 7
days a week), nor is it consistent (i.e. the power cuts are predictable). The
office has a site generator, but it only runs during work hours (except for
scheduled breaks) because the cost of fuel is so high. So some colleagues have
designed a solar panel + battery + inverter system to try to bridge cuts up
to 24 hours *just for the satellite internet system*. It's cost is
estimated to be around 20% of the already astronomical costs of the satellite
internet service itself.

This is just one example of why working in the developing world can be so
expensive: you're frequently carrying significant additional costs compared with
the developed world because you often find yourself creating your own
infrastructure on which to base the services you are providing.
