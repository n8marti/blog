---
title: 'More "Trial" than "Tech"'
Date: 2023-05-13
tags: ["hot"]
draft: false
---

I'm taking a detour today to highlight one of the reasons that I often have a hard time being productive, especially during the afternoon hours.
<!--more-->

### It's all about the Heat Index

It's no secret that nearly *everyone* working in a tropical climate feels lethargic during *most* afternoons of the year.
We chalk it up to warm to hot temperatures and high humidity, which is subjectively true, but I've never taken the time to consider an objective measurement of this until now.

The [**Heat Index**](https://en.wikipedia.org/wiki/Heat_index) is calculated using temperature and relative humidity. The following plot shows the levels of discomfort or danger at various combinations of temperature and humidity.

>![Heat Intex plot](/img/heat-index-plot.svg)
>Red: Extreme Danger: heat stroke is imminent.  
>Orange: Danger: heat cramps and heat exhaustion are likely; heat stroke is probable with continued activity.  
>Dark Yellow: Extreme caution: heat cramps and heat exhaustion are possible. Continuing activity could result in heat stroke.  
>Yellow: Caution: fatigue is possible with prolonged exposure and activity. Continuing activity could result in heat cramps.  
>https://en.wikipedia.org/wiki/Heat_index

Now lets take into consideration typical temperatures and relative humidity levels here in the Central African Republic.

### Temperature

The high temperature *inside our apartment* during the hottest days of the year is 32-33°C (90-92°F). Otherwise, most of the year it's still around 30°C (86°F). For comparison, today it rained all morning and is an especially cool day, at least by my reckoning. It's still 28°C (84°F) inside as I type at 2pm.

| | Temp | RH | H. Index | level |
| :-- | --: | --: | --: | :-: |
| coolest 10% | 28°C | - | - | - |
| typical 80% | 30°C | - | - | - |
| hottest 10% | 33°C | - | - | - |

### Humidity

Meanwhile, the humidity also stays pretty high most of the year. We don't have a barometer, so I'm going to use climate average numbers for this. But you'll be able to see that it probably doesn't make much difference. I got the following numbers from [timeanddate.com](https://www.timeanddate.com/weather/central-african-republic/bangui/climate).

During the hottest, driest part of the year (Feb. & Mar.) the relative humidity is more or less between 65% and 70%. During the wettest times of the year the relative humidity is typically about 85%. And *most* of the rest of the time it's in the 80% range.

This is a gross oversimplification, but I'd say it's safe to assume that the 10% coolest days also have the highest humidity because they tend to be rainy days. The 10% hottest days have the lowest, and days of typical temperatures also have typical humidity. So let's find our points on the chart above and fill in the rest of the table now:

| | Temp | RH | H. Index | level |
| :-- | --: | --: | --: | :-: |
| coolest 10% | 28°C | 85% | 33°C | dark yellow |
| typical 80% | 30°C | 80% | 38°C | dark yellow |
| hottest 10% | 33°C | 65% | 41.5°C | orange |

### Conclusion

So there you have it:
- 90% of the year we're in dark yellow, where *"heat cramps and heat exhaustion are possible. Continuing activity could result in heat stroke"*
- and the hotter 10% of the year we're in orange, where *"heat cramps and heat exhaustion are likely; heat stroke is probable with continued activity"*!

No wonder it can be hard to make it through the afternoon around here!
