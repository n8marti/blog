---
title: Online Collaboration
Date: 2022-03-19
tags: ["collaboration", "Google"]
draft: false
---

It's tricky when you have an intermittent internet connection.

A colleague and I needed to work on a document together, and our organization
uses [Google Workspace](workspace.google.com) for collaboration, which includes
[Google Docs](docs.google.com). <!--more-->
Millions of people take advantage of the features
it includes, including live, simultaneous editing of the same document. So we
created a Google doc to work on together. But then he told me that his computer
didn't work well with Google Docs (mostly due to his intermittent internet
connection).

In the end we sat next to each other at a table. I opened the document on my
laptop, and we proceeded to pass it back and forth between us so that we could
each take turns editing the same document.

***COLLABORATION!***
