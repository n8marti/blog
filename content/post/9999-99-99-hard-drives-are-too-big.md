---
title: Hard Drives Are Too Big
Date: 9999-99-99
tags: ["cli", "gparted", "hard-drive", "linux", "recovery", "windows"]
draft: true
---

There are many ways to subvert a computer's operating system to suit your own purposes. This is just one of them.
<!--more-->

### Sometimes subversion gets you to the best solution

I spent all day the other day trying to "move" a hard drive from one computer to another one. Here are the steps that I had to take:

1. I actually wanted to move the old hard drive's contents to a newer one, so I first wanted to clone the old HDD onto the new SSD. But it turned out that the newer SSD I had was smaller than the older HDD, which is common, so a straight clone was out of the question.
1. I was already frustrated at how unnecessarily big the old HDD was (it was only about 10% full), so I decided to just simply put it direclty into the newer laptop, skipping the cloning step and giving up on using the newer SSD.
1. However, it *also* turned out that the old HDD was physically too thick to fit in the newer computer's housing. Trust me, I pushed and shoved on the laptop's back cover, trying to force it back on, but it just wouldn't go. So the old HDD was too big in terms of capacity to make a clone, and it was lierally too big physically for a straight transplant.
1. So, time to get a little more creative. Since the old HDD had plenty of empty space, I decided to shrink down its main partition so that it would ultimately fit onto the new, smaller SSD. Here's where **gparted** comes in handy.
1. But before trying to shrink the partition, I've learned from experience that it's worth the effort to create a backup image* of the disk first. Buuuuuut, it turned out that I didn't have adequate space on my 2 TB external drive.
(*An image is basically a byte-for-byte copy, rather than a file-for-file copy. It's a good choice when multiple partitions are involved.)
1. So I next had to make room on my 2 TB drive by moving hundreds of GB worth of files and disk images to various other, smaller drives (this took hours--all morning!).
1. I actually started making the backup image at around 11:30am. *That* took about 3 hours.
1. Then I had to do a bunch of arithmetic to figure out how small the C: drive needed to be. (actual disk and partition sizes in bytes; make sure C: drive contents would fit, etc.)
1. Then I got to work with **gparted**. I needed to do 2 steps: shrink the C: drive partition, and move the D: drive partition towards the front of the physical disk so that all the empty space was at the end. Unfortunately, **gparted** told me that the drive had errors and that I needed to run chkdsk in Windows to fix it. Great. (NOTE: insert image of output)
1. I boot up the HDD in the old laptop, launch a CMD prompt as Admin, and schedule a chkdsk /f for the next boot (can't run it on a drive that's in use).
1. Rebooted, let it run for several minutes, then rebooted a couple more times, per the advice given to me by **gparted**.
1. Resized C: Drive, moved D: drive (several minutes); no errors!
1. used **sgdisk** to copy the GPT partition from the old HDD to the new SDD, based on advice from https://unix.stackexchange.com/a/574262
1. used dd to send image of each partition from old HDD to new SDD; sudo dd if=/dev/sda2 of=/dev/sdb2 bs=1M status=progress conv=notrunc,noerror,sync
1. finally done at around 6:30pm!

If the old HDD had been as small or smaller than the new SSD (256 GB), this would have only taken me about 2 hours to do. Instead, because the blasted thing was 750 GB (mostly unused!), it took all stinking day!