---
title: 'Zoom Is Faster Than Meet'
Date: 2023-09-23
tags: ["google", "internet", "network", "remote-teams"]
draft: false
---

Zoom is much more usable than Google Meet in a place with significant network
latency. Here's why.
<!--more-->

Both [Zoom](https://zoom.us) and [Google Meet](https://meet.google.com) provide
excellent video conferencing solutions with lots of useful features. But where I
am (in the developing world), where internet latency is rather high, the feature
that matters most is simply the ability to set up a call. But before we can talk
about why latency has a big effect you need to have a clear idea of what it is.

### What is "network latency"?

To define latency you first need a basic picture of how internet communication
works. Communication between your device and an internet server is comprised of
discrete packets of data that get sent back and forth in sequence, like a series
of direct messages. Latency is the amount of time it takes for your device
to send a message to a server, for the server to create and send its own message
in response, and then for your device to receive the response. Latency is
typically measured in milliseconds (ms). Here are some real-time latency figures,
measured on September 21, 2023:

![WonderNetwork worldwide pings](/img/pings.png)
> Source: https://wondernetwork.com/pings/

You can see that latency times vary from 100ms to 300ms between the cities shown
in this image. 100ms is actually still considered fairly slow. Even Starlink,
which uses Low-Earth Orbit satellites instead of physical transmission lines,
[consistently registers](https://www.pcmag.com/news/starlink-speed-tests-2023-vs-2022)
less than 50ms.
 
You can see the effects of higher latency if you imagine your device sending and
receiving a series of 20 packets to a server. If latency is 50ms these 20 round
trips will take about 1 second. However, if your latency is 500ms, then your device
would require 10 seconds for the same communication, regardless of your
[bandwidth](https://n8marti.gitlab.io/blog/post/2021-10-10-need-more-bandwidth/).

On the other hand, if each of the 20 packets contained communication that was
independent of all the others, then they could all be sent at once and the responses
would all be received at once in the amount of time equal to the connection's
latency (assuming that your bandwidth is unlimited). However, setting up web-based
calls doesn't seem to work this way.
 
So now let's look specifically at **Zoom** and **Meet**.

### Setting up an audio call: Zoom vs. Google Meet

"Setting up an audio call" basically just means that your device sends a bunch of
messages (packets) to some servers hosted by the videoconferencing provider. The
servers, then, send responses to these messages. A large number of these messages
are of the dependent and sequential kind rather than independent and concurrent kind.

Once all the right communication has happened and the right connections are made
you have entered the meeting "room" and are able to communicate with anyone else
who is there.

I've noticed for a long time that it's much easier here to start a **Zoom** call than
it is to start a **Meet** call. Hard enough, in fact, that **Meet** more often than not times out and
fails to establish the call. That's unfortunate because our organization has
Google Workspace accounts, and **Meet** provides unlimited meeting length compared
with **Zoom**, whose free plan currently has a 40-min limit for even one-on-one calls.
But we have a hard time using **Meet** because of how unreliable it is when starting a call.

### The test

So I finally decided to gather some specific data related to starting a call using
each service. I typically use the **Zoom** desktop app, while **Meet** only offers a web
interface. So in order to compare apples to apples, I decided to begin a call in
a browser with each service and record the network traffic generated.

I opened a new Firefox tab, then opened the Developer Tools Network log (Ctrl+Shift+E).
This tracks all network communications relevant to the tab that was just opened.
This is what you see if you have Network Tools open while loading [DuckDuckGo](https://duckduckgo.com):

![Firefox Network Tools](/img/firefox-network-tools.png)

Here you see a list of all communications sent between my device and the DuckDuckGo
server while loading the page. You also see on the right side of each line the
response code given by the server (often "200", which means "success") and, crucially,
the latency, or response time, for the communcation.

I did the same thing while beginning both a **Zoom** call and a **Meet** call. Then I
saved each log as a text file for further review, `zoom.txt` and `meet.txt`. I
used command-line tools to evaluate the contents of each file. Here's a sample
of what's in these files:
```shell
$ cat zoom.txt
06:26:36.530 XHRPOST
https://log-gateway.zoom.us/nws/join/logger/wjmf

06:26:36.641 POST
https://zoom-privacy.my.onetrust.com/request/v1/consentreceipts
[HTTP/2 201 Created 1282ms]

06:26:36.642 GET
https://sil.zoom.us/wc/join/7347711612?pwd=ekdOekkyL0l3WXdpTFZqZlg4Mi9yQT09
[HTTP/2 302 Found 1281ms]

[...]
```

The first thing I wanted to know was whether or not both services made a similar
number of server connections.

#### How many individual communications happened?
```shell
$ cat meet.txt | grep -Eo '^[a-z]+://[^/]*/' # shows all lines with a web address
https://meet.google.com/
https://www.gstatic.com/
https://lh3.googleusercontent.com/
[...]
$ cat meet.txt | grep -Eo '^[a-z]+://[^/]*/' | wc -l # counts # of lines
194
$ cat zoom.txt | grep -Eo '^[a-z]+://[^/]*/' | wc -l
63
$
```

So **Meet** made 194 connections while setting up a call, while **Zoom** only made 63.

#### How long did these connections take?
```shell
$ cat meet.txt | grep -E 'ms\]'
[HTTP/2 200 OK 1243ms]
[HTTP/2 200 OK 1475ms]
[HTTP/2 200 OK 3759ms]
[HTTP/2 200 OK 0ms]
[HTTP/3 200 OK 1294ms]
[HTTP/3 200 OK 7535ms]
[HTTP/3 200 OK 3725ms]
[HTTP/3 200 OK 1603ms]
[...]
$ cat zoom.txt | grep -E 'ms\]'
[HTTP/2 201 Created 1282ms]
[HTTP/2 302 Found 1281ms]
[HTTP/2 302 Found 818ms]
[HTTP/2 200 OK 883ms]
[HTTP/3 200 OK 1872ms]
[HTTP/2 200 OK 744ms]
[HTTP/2 200 OK 1117ms]
[HTTP/2 200 OK 1099ms]
[...]
```

**Meet** and **Zoom** were pretty similar, taking between 1000ms to 2000ms for most
connections. Here are the actual averages:  
Meet = 1565ms, Zoom = 1726ms  
That is *way* higher than what we saw in the ping table above, and even 30 times
higher than a "good" connection via Starlink!

Also, since each log entry includes a timestamp, we can look at the first and last
entries to see how long the entire process took:
```shell
$ cat meet.txt | head -n4
06:18:42.078 GET
https://meet.google.com/mrr-pogb-qqh
[HTTP/2 200 OK 1243ms]

$ cat meet.txt | tail -n4
06:22:05.724 XHRPOST
https://meet.google.com/hangouts/v1_meetings/media_sessions/callperf?key=AIzaSyCG_6Rm6c7ucLr2NwAq33-vluCp2VfSkf0&alt=proto
[HTTP/3 200 OK 723ms]

```
So in this experiment (which took place early in the morning, as you can see, 
when the network is less congested) **Meet** took 3 min 23 sec to start the meeting. The
page started loading at 06:18:42 and finished at 06:22:05. You can imagine that
if the network is more congested, which results in lost packets, which results in
greater apparent latency, then the **Meet** servers' code for handling errors could
easily assume that the connection is broken and lead to server timeouts.

**Zoom**, on the other hand only took 38 sec to start its meeting (06:26:36 to 06:27:14):
```shell
$ cat zoom.txt | head -n3
06:26:36.530 XHRPOST
https://log-gateway.zoom.us/nws/join/logger/wjmf

$ cat zoom.txt | tail -n4
06:27:14.378 GET
wss://zoomdv8512941rwg.dv.zoom.us/wc/media/7347711612?type=s&cid=724CA3D5-421A-9FDC-685A-0E9F6743A3BC&mode=2
[HTTP/1.1 101 Switching Protocols 2035ms]

```

#### Summary table

| | latency | connections | setup time
| :--- | ---: | ---: | ---:
| Meet | 1565ms | 194 | 3m23s
| Zoom | 1726ms | 63 | 38s

### The conclusion

So my theory is that **Meet**, with about 3 times as many server connections to make,
and with very high latency here in general, is just much more prone to connection problems
and timeouts while initializing the audio call than **Zoom** is.

If Google were to
simplify their setup process to require fewer server connections, then I assume
we would have a better experience here with **Meet** (as well as all the other Google Workspace apps).
But, I imagine that we are hidden away in the very cornerest of corner cases
of Google's user base, and they're not likely to consider our kind of situation
when optimizing their systems.
