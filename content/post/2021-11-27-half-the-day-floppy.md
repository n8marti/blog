---
title: Floppy Disk Reader Fail
Date: 2021-11-27
tags: ["recovery", "floppy disk", "project", "fail", "tape"]
draft: false
---

Have you ever spent half the day digging through old computers and parts to find
something with a floppy disk reader?
<!--more-->

And did you find an external floppy disk reader with some ancient pseudo-serial/pseudo-USB
connection that didn't fit any port on any other device, so you decided to
dismantle it and take out the floppy drive and ribbon to see if you could
directly install it in an old desktop PC?

And did you realize that the only place it could possibly go was in place of the
PC's CD drive, so you also dismantled the drive so that the floppy drive could
sit inside the CD drive's casing so that it could be slid back into the PC? But
then did you then realize that you needed to use masking tape to hold it all
together so that it would properly slide back into its slot?

And did you get to the final glorious moment where you hooked up the old PC,
to a power supply, monitor and keyboard, and fired it up, only to
realize the motherboard was bad and there was no way to get it to boot?

Me neither...
