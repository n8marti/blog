---
title: How to Use Traffic Shaper on a pfSense Firewall
Date: 9999-99-99
tags: ["firewall", "network", "internet", "pfSense", "open source"]
draft: true
---

### Proper Bandwidth Management
1. Determine actual upload and download bandwidth.
  - iperf3 for throughput testing
1. Manage bandwidth usage on outgoing traffic of WAN device (or on incoming traffic of LAN device).
  - Managing incoming traffic on WAN device (most intuitive idea) results in packets being sent all the way through the limited WAN "pipe", only to be queued at the relatively wide-open firewall-to-LAN "pipe".
  - Accordingly, Max. bandwidth property in Traffic Shaper should correspond to the effective max. bandwidth seen on the upload side (outgoing from WAN) of the firewall, *not* the max. bandwidth seen on the download side (incoming to WAN).
1. I set up 6 queues to allot a specific amount of bandwith for 6 different essential purposes:
  1. qWeb_work
  1. qAV_calls
  1. qComms
  1. qSupport
  1. qDefault
  1. qACK
1. I used floating firewall rules to assign certain traffic to each queue.
1. I set up several aliases to group similar kinds of traffic together when applying the firewall rules.


---

I recently wanted to see if I could get better network performance while using a
pfSense firewall. Here's what I learned while configuring and testing the built-in
Traffic Shaper feature.
<!--more-->

### Goal
Use Traffic Shaper to allocate how available internet (WAN) bandwidth gets used.

### Conditions
- Traffic Shaper requires that a hard limit be set for the total amount of bandwidth
  allowed to and from the WAN. In my case for testing purposes I use a very
  restrictive limit of 1024 Kbps, but this should be set to the allotted bandwidth
  given to you by your ISP, if your service includes such a thing, or maybe the
  average bandwidth your ISP provides during the work day.
- Fine control over bandwidth management requires relatively stable and predictable
  IP addresses or ports for the traffic being managed. Unfortunately, this makes
  traffic shaping somewhat unreliable on big CDN sites who make use of myriad domain
  names that are hard to predict and that can resolve to frequently-changing,
  undocumented IP addresses.

My purpose here is not to give a comprehensive guide but to show the basic steps
I've used for configuring Traffic Shaper and evaluating the results. I would
call it a *proof of concept* more than a guide.

### Initial Setup
- Wizard config
- 4 queues

### Configuration
- queues
  - qHigh: GMail
  - qLow: Facebook
  - qDefault
  - qTesting
- aliases
  - GMail
  - Facebook

### Testing
- firewall rules
  - sort by queue, add dividers for easier comprehension
  - put one rule at a time in testing queue
- queue status
  - observe whether or not there's traffic in the testing queue
  - try loading pages from sites in both queues at the same time and observe the
    bandwidth usage

### References
- [Traffic Shaper offical documentation](https://docs.netgate.com/pfsense/en/latest/trafficshaper/index.html)
- [Sample config file](img/config.xml)
