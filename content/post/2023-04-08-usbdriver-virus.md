---
title: "The Ubiquitous USBDriver.exe Virus"
Date: 2023-04-08
tags: ["cli", "linux", "recovery", "usb-drive", "virus", "windows"]
draft: false
---

We have a pesky but (thankfully) predominantly powerless virus proliferating among our computers and USB drives.
<!--more-->

A few weeks back [I wrote](/post/2023-03-18-distracted-destruction) about how I accidentally wiped my 400 GB storage SD card. I'm still a little down about that. But now I'll share more about the virus that was at the root of the problem.

### The Perp

I don't know if this virus has an official name, but I'll call it the USBDriver.exe virus. Who knows where exactly it came from, but, like many viruses around here, it's most likely proliferating in the open-air markets and ad-hoc tech support kiosks, whose services can include printing services, "free" movies and software, hardware repairs, etc. Most likely this virus makes it onto my colleagues' USB drives when they share files for printing at these types of establishments.

### His M.O.

This virus is pretty simple, and it affects both USB drives and Windows PCs as carriers. Here's the basic process:
- An infected Windows PC is running a background process called "MSBuild.exe".
- This process apparently moves all the files on a target USB drive to somewhere deep into a series of new, nameless folders that don't even show up in Windows File Explorer.
- Even if you manage to see the folders (by running a Windows command to change attributes, or by putting the USB drive in a Linux system), there are several layers and subfolders that all appear to be nameless and otherwise empty. Once you've navigated down a couple of layers it's easy to assume that the files are actually gone and all these folders are empty decoys.
- What you *do* see, though, is a file called "USBDriver.exe". A "non-techy" user can be forgiven for assuming that double-clicking this file would magically install your apparently "missing" USB driver.
- Bust as soon as you double-click on "USBDriver.exe", boom! You've just installed the virus on *your* computer, completing the cycle.

### The cure

The files on the USB are still there, though, and can be found easily enough if you know where and how to look for them. You can run a special command in Windows (see the YouTube video link below), or you can just plug the USB drive into a Linux system. At this point you can either use a fancy terminal command to recursively search the folders and find all files, or you can just navigate to them (thanks to the info in the YouTube video), because it turns out the files are *always* moved to this location: 1st no-name folder > 1st no-name folder > 5th no-name folder > 3rd no-name folder. Maybe this file tree (Linux command) will help you visualize that:
```shell
$ tree /path/to/USB/drive
/path/to/USB/drive/
├──  # 1st no-name folder
│   ├── ‌ # 1st no-name folder
│   │   ├── ‌ # 1st no-name folder
│   │   │   ├── ‌
│   │   │   ├── ‍
│   │   │   ├── ‬
│   │   │   ├── ⁪
│   │   │   ├── ⁫
│   │   │   ├── ⁮
│   │   │   └── ⁯
│   │   ├──  # 2nd no-name folder‍
│   │   │   ├── ‌
│   │   │   ├── ‍
│   │   │   ├── ‬
│   │   │   ├── ⁪
│   │   │   ├── ⁫
│   │   │   ├── ⁮
│   │   │   └── ⁯
│   │   ├── ‬ # 3rd no-name folder
│   │   │   ├── ‌
│   │   │   ├── ‍
│   │   │   ├── ‬
│   │   │   ├── ⁪
│   │   │   ├── ⁫
│   │   │   ├── ⁮
│   │   │   └── ⁯
│   │   ├──  # 4th no-name folder⁪
│   │   │   ├── ‌
│   │   │   ├── ‍
│   │   │   ├── ‬
│   │   │   ├── ⁪
│   │   │   ├── ⁫
│   │   │   ├── ⁮
│   │   │   └── ⁯
│   │   ├──  # 5th no-name folder⁫
│   │   │   ├── ‌ # 1st no-name folder
│   │   │   ├── ‍ # 2nd no-name folder
│   │   │   ├── ‬ # 3rd no-name folder
│   │   │   │   ├── File1 # <-- Aha! the files!
│   │   │   │   ├── File2
│   │   │   │   ├── ...

```

You can simply navigate to this folder in the File Explorer, then cut/paste them back to the top level of the USB drive. I prefer to move them to another disk first, then format the USB drive, then move them back.

But saving the files is only part of the cure. If you actually clicked on the USBDriver.exe file, then your computer still needs to be disinfected! Thankfully, again, this is not terribly complicated, once you know where to look. You need to:
1. Stop the MSBuild.exe process by finding and ending it in Task Manager.
1. Delete the MSBuild.exe executable (and any accompanying files) from your system. These files are found in C:\Users\Public\Libraries.

### Follow-up thoughts

Apparently, Windows Security fails to recognize this virus. Several weeks ago I found a system infected with it, but at that point I didn't know how to manually get rid of it. I did both a quick scan and a full scan of the system using Windows Security, and it never found anything. I ended up wiping and reinstalling the whole system instead. It's possible that the default settings for Windows Security simply don't scan the correct folder (even when doing a "full" scan?). That maybe needs more research.

Many of our users use ESET, and I haven't yet had the chance to find out if ESET correctly recognizes the virus file(s) or not. Also needs further research.

I deal with this virus on USB drives *at least* once every couple of months, but I hadn't knowingly come across it on a Windows system until fairly recently. So I'm glad I took the time to find out how to properly get rid of it from an infected system to avoid having to do more reinstalls in the future (although a reinstall can be good for other reasons, too).

Lastly, here's the YouTube video I found that gives (in a painstakingly slow presentation) the steps for proper removal from infected USB drives and Windows systems: https://www.youtube.com/watch?v=RnlLFbgKNmQ