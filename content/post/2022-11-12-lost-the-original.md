---
title: "This PDF is all I have!"
Date: 2022-11-12
tags: ["cli", "ocr", "recovery", "open source", "pdf", "machine learning", "libreoffice"]
draft: false
---

This week a colleague came to me with a request: He had a PDF of a document
published in 2011 that he wanted to update, but he didn't have the original
source document. *No problem,* I thought to myself, *I'll just open it in LibreOffice Draw...*
<!--more-->

### Just open it in LibreOffice...

I got the file from him and did just that. I've done this a few times for other
generated PDFs, and I had no doubts about it working just as well this time.
**Draw** is part of the open-source, license-free [LibreOffice Suite](https://www.libreoffice.org/discover/libreoffice/).

![original file](/img/ocr-0-orig.png)
***The original PDF document***

![opened in Draw](/img/ocr-1-draw.png)
***The document opened in LibreOffice Draw***

Yuck. That's not what I expected. What's going on here? The characters from the
PDF aren't showing up correctly when shown by the system. Maybe some weird formatting
issue with the file? Maybe I just need to get the text separately then re-insert
it into this PDF.

### Okay, let's just copy/paste the text out and rebuild the PDF...

![copy the text](/img/ocr-2-copy.png)

![paste the text](/img/ocr-3-paste.png)

Great, same issue. Maybe I need something "smarter" than just copy/paste?
Enter: ```pdftotext```.

### What if we extract the text using a command?

![extract the text](/img/ocr-4-pdftotext.png)

Fail! So...maybe this is really a font issue? I don't really know what to do about that,
but it seems like the crux of the problem is there somewhere.

### How about we extract the *fonts* and install them first?

Well, after much searching and reading about how PDFs work, I found out that the
PDF used "Custom" fonts, so then I converted the file to a PostScript file,
which then let me find out how the fonts were defined inside.
(I'll spare you the details, because this was ultimately a fruitless effort.)
Then I used that info to create my own font definition files, I dropped them
into my system's fonts folder, and...nothing.

It turns out that the "Custom" fonts in the PDF are defined in a roundabout way
that eventually refers back to character images embedded within the file. This
quickly went beyond my level of motivation to try to figure out how to make that
work at a system level. So ultimately the font extraction idea was 2-hour dead-end.

![list the fonts](/img/ocr-5-pdffonts.png)
***Listing the "Custom" fonts embedded in the PDF***

### Fine, let's OCR the thing and see how that goes

So my last-ditch effort (*where does this term come from?*) involved:
- converting the PDF pages to images (```pdftoppm -png [pdf_file]```),
- then using Optical Character Recognition (OCR) to "scrape" the text from the images
  (```tesseract [image_file] [output_base_name] -l Latin```),
- then re-inserting that text back into the Draw file,
- then having to redo all the text layout.

Here's what I ended up with:

![OCR text](/img/ocr-6-tesseract.png)
***Extracting text with tesseract***

![final document](/img/ocr-7-result.png)
***The final document***

The final document ended up with about 95% of the correct characters, and about
a 95% fidelity to the original layout. It's not perfect, but it's now in a state
where my colleague can edit the text and create an updated version.

### A note about OCR

The challenge with OCR software is that you have to identify the language
associated with the text in the image, and the software's machine learning engine
needs to be trained on existing data already in that language. This can't happen
with low-resource languages that have little-to-no existing documents, such as Banda-Linda.
Thankfully, ```tesseract```,
([GitHub page](https://github.com/tesseract-ocr/tesseract)) gives you the option of choosing
a writing script rather than a language. The Latin script covers *most* of the characters
used in the Banda-Linda language, so that's how I got my estimated 95% of the
correct characters.

Re-inserting the text back into the document was also a fairly tedious task, but
that's not the focus of this post. In the end this project took about 6 hours to
complete, but the next time this situation comes up I have a clearer system to
follow that should let me produce the text in just a few minutes, and then whatever
time would be required to re-insert the text into the document and redo the layout.
