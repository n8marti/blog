---
title: About me
subtitle: I'm keeping it brief
comments: false
---

I work in the IT world. Most of my time is spent managing desktops, servers, and
networking for a small organization in the developing world. I also do some
basic development for an Ubuntu-based distribution called
[wasta [Linux]](https://wastalinux.org), which is tailored for people who work
in the domains of Applied Linguistics and Translation for minority languages.
